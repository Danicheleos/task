function add_cell(data) {
    return '<td>'
        +data
    '</td>'
}

function add_image(data){
    return '<td style="text-align: center; width: 200px;">'
        +'<img src ="data:image/png;base64,'+data+'" style="width: 200px; height: auto;"/>'
    '</td>'
}

fetch("/api/contractors")
    .then(response => response.json())
    .then(contractors => {
    contractors.forEach(contractor => {
    const el = document.createElement('tr')
    a =add_image(contractor.image)
    a += add_cell(contractor.name)
    a += add_cell(contractor.phone)
    a += add_cell(contractor.address)
    a += add_cell('<a href="../../contractor/edit/' + contractor.id + '/">edit</a>')
    a += add_cell('<a href="../../contractor/delete/' + contractor.id + '/">delete</a>')
    el.innerHTML = a + '</tr>'
document.querySelector('#main_table').append(el)
})
})

