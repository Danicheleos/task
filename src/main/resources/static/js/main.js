function add_cell(data) {
    return '<td>'
        +data
    '</td>'
}

fetch("/api/contracts")
    .then(response => response.json())
    .then(contracts => {
        contracts.forEach(contract => {
            const el = document.createElement('tr')
            a = add_cell('<input type="checkbox" name="export" value="'+contract.id+'">')
            a += add_cell(contract.contractor_name)
            a += add_cell(contract.name)
            a += add_cell(contract.date)
            a += add_cell(contract.start)
            a += add_cell(contract.end)
            a += add_cell(contract.cost)
            a += add_cell('<a href="../../edit/' + contract.id + '/">edit</a>')
            a += add_cell('<a href="../../delete/' + contract.id + '/">delete</a>')
            a += add_cell('<a href="../../contract/' + contract.id + '/">open</a>')
            el.innerHTML = a + '</tr>'
            document.querySelector('#main_table').append(el)
        })
    })

