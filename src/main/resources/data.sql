

CREATE USER 'killbill'@'localhost' IDENTIFIED BY 'hellodatabase';
GRANT ALL PRIVILEGES ON Contracts.* TO 'killbill'@'localhost';

DROP DATABASE IF EXISTS contracts;
CREATE DATABASE contracts;
USE contracts;


CREATE TABLE contract
(
      id bigint(20) auto_increment,
      name varchar(255),
      date date,
      start date,
      end date,
      cost double,
      PRIMARY KEY (id)
);


CREATE TABLE stage
(
      id bigint(20) auto_increment,
      contract_id bigint(20),
      name varchar(255),
      start date,
      end date,
      cost double,
      purchase_date date,
      PRIMARY KEY (id),
      FOREIGN KEY (contract_id) REFERENCES contract(id)
);
