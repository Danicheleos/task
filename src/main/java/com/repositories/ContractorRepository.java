package com.repositories;


import com.domains.Contractor;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ContractorRepository extends JpaRepository<Contractor, Long> {
    Contractor findByName(String name);
}
