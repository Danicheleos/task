package com.repositories;

import com.domains.Contract;
import com.domains.Stage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;


public interface StageRepository extends JpaRepository<Stage, Long> {

}
