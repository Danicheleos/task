package com.validators;

import com.domains.Contract;
import com.domains.Stage;
import com.repositories.StageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
@Service
public class ContractValidator {
    @Autowired
    private final StageRepository stageRepository;


    public ContractValidator(StageRepository stageRepository){
        this.stageRepository = stageRepository;
    }


    public String validation(Contract contract){
        if (checkName(contract.getName())){
            return "ERROR WRONG NAME DATA";
        }
        if (checkCost(contract.getCost())){
            return "ERROR WRONG COST DATA";
        }
        if (checkAfter(contract.getStart(),contract.getEnd())){
            return "ERROR WRONG START/END DATA";
        }
        if (checkAfter(contract.getDate(),contract.getStart())){
            return "ERROR WRONG DATE/START DATA";
        }
        if (checkDateNull(contract.getDate())
                ||checkDateNull(contract.getEnd())
                ||checkDateNull(contract.getEnd())){
            return "ERROR WRONG DATA(NULL)";
        }

        if (!checkForStages(contract)){
            return "Stages dates out of range";
        }
        return "";
    }

    private boolean checkName(String Name){
        return Name.equals("");
    }

    private boolean checkCost(Double cost){
        return cost<0;
    }

    private boolean checkAfter(Date start, Date end){
        return start.after(end);
    }

    private boolean checkDateNull(Date date){
        return date==null;
    }

    private boolean checkForStages(Contract contract){
        for (Stage stage: stageRepository.findAll()){
            if(stage.getContractId().equals(contract.getId())){
                System.out.println(contract.getStart()+" "+stage.getStart());
                if (contract.getStart().after(stage.getStart())) return false;
                System.out.println(contract.getEnd()+" "+stage.getEnd());
                if (contract.getEnd().before(stage.getEnd())) return false;

            }
        }
        return true;
    }


}
