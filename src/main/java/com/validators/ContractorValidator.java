package com.validators;

import com.domains.Contractor;
import com.repositories.ContractorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContractorValidator {

    @Autowired
    private ContractorRepository contractorRepository;

    public String validation(Contractor contractor){
        if (checkName(contractor)){
            return "ERROR. NAME DATA MUST BE UNIQUE";
        }

        return "";
    }

    private boolean checkName(Contractor contractor){
        if(contractor.getName().equals("")) return true;
        for(Contractor contractor_id: contractorRepository.findAll()){
            if(contractor.getName().equals(contractor_id.getName())&&contractor.getId()!=contractor_id.getId()) return true;
        }
        return false;
    }
}

