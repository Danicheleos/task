package com.validators;


import com.domains.Stage;
import com.repositories.ContractRepository;
import com.repositories.StageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class StageValidator {
    @Autowired
    private final ContractRepository contractRepository;
    @Autowired
    private final StageRepository stageRepository;


    public StageValidator(ContractRepository contractRepository, StageRepository stageRepository){
        this.stageRepository = stageRepository;
        this.contractRepository = contractRepository;
    }


    public String validation(Stage stage){
            if (checkName(stage.getName())){
                return "ERROR WRONG NAME DATA";
            }
            if (checkCost(stage.getCost())){
                return "ERROR WRONG COST DATA";
            }

            if (checkBefore(stage.getEnd(),stage.getStart())){
                return "ERROR WRONG START/END DATA";
            }

            if (checkBefore(stage.getStart(),contractRepository.findById(stage.getContractId()).get().getStart())){
                return "ERROR WRONG START IS BEFORE CONTRACT START";
            }

            if (checkBefore(contractRepository.findById(stage.getContractId()).get().getEnd(),stage.getEnd())){
                return "ERROR WRONG END IS AFTER CONTRACT END";
            }
            if (checkBefore(stage.getEnd(),(stage.getPurchaseDate()))){
                return "ERROR WRONG PURCHASE DATE IS AFTER STAGE END";
            }
            if (checkStages(stage)){
                return "ERROR DATES ARE CROSSING ANOTHER STAGES";
            }
            if (checkDateNull(stage.getStart())
                    ||checkDateNull(stage.getEnd())
                    ||checkDateNull(stage.getPurchaseDate())){
                return "ERROR WRONG NULL DATE";
            }
            return "";
        }

    private boolean checkName(String Name){
        return Name.equals("");
    }

    private boolean checkCost(Double cost){
        return cost<0;
    }

    private boolean checkBefore(Date start, Date end){
        return start.before(end);
    }

    private boolean checkDateNull(Date date){
        return date==null;
    }

    private boolean checkStages(Stage stage){
        Date start = stage.getStart();
        Date end = stage.getEnd();
        for (Stage stage_id: stageRepository.findAll()){
            if(stage_id.getContractId()==stage.getContractId()) {
                if (stage_id.getStart().after(start) && stage_id.getEnd().before(end)&&stage_id.getId()!=stage.getId()) return true;
                if (stage_id.getStart().before(start) && stage_id.getEnd().after(start)&&stage_id.getId()!=stage.getId()) return true;
                if (stage_id.getStart().before(end) && stage_id.getEnd().after(end)&&stage_id.getId()!=stage.getId()) return true;
                if (stage_id.getStart().equals(start) && stage_id.getEnd().equals(end)&&stage_id.getId()!=stage.getId()) return true;
            }
        }
        return false;
    }

}
