package com.controller;

import com.controller.services.StageServices;
import com.domains.Stage;
import com.repositories.ContractRepository;
import com.repositories.StageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@Controller
public class StageController {
    @Autowired
    StageServices stageServices;

    public StageController(ContractRepository contractRepository, StageRepository stageRepository) {
        stageServices = new StageServices(contractRepository,stageRepository);
    }


    @GetMapping("/contract/{contractID}")
    public String stagesOpen(@PathVariable Long contractID, Model model){
        model.addAttribute("contactID",contractID);
        model.addAttribute("welcome","editing contract");
        return "stages";
    }
    @GetMapping("/contract/{contractID}/delete/{stageID}")
    public String stagesDell(@PathVariable Long stageID, @PathVariable Long contractID, Model model){
        stageServices.remove(stageID);
        model.addAttribute(contractID);
        return "stages";
    }

    @GetMapping("/contract/{contractID}/add")
    public String stagesAdd(@PathVariable Long contractID, Model model){

        model.addAttribute("contractID", contractID);
        model.addAttribute("stageID", 0);
        model.addAttribute("stage", stageServices.newStage(contractID));
        return "editStage";
    }

    @GetMapping("/contract/{contractID}/edit/{stageID}")
    public String stagesEdit(@PathVariable Long stageID, @PathVariable Long contractID, Model model){
        model.addAttribute("contractID", contractID);
        model.addAttribute("stageID", stageID);
        model.addAttribute("stage", stageServices.getById(stageID));
        return "editStage";
    }

    @PostMapping("/contract/{contractID}/edit/{stageID}")
    public String stagesEdit(@PathVariable Long stageID, @PathVariable Long contractID, Model model, Stage stage){
        model.addAttribute("contractID",contractID);
        String check = stageServices.check(stage);
        if (!check.equals("")){
            model.addAttribute("error",check);
            model.addAttribute("stage",stage);
            return "editStage";
        }

        if(stageID>0) stage.setId(stageID);
        stageServices.save(stage);
        return "stages";
    }
}
