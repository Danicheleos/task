package com.controller;

import com.controller.services.ContractServices;
import com.controller.services.ContractorServices;
import com.domains.Contractor;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;


@Controller

public class ContractorController{
    @Autowired
    ContractorServices contractorServices;
    @Autowired
    ContractServices contractServices;


    @GetMapping("/add_contractor")
    public String add_contractor(Model model){
        model.addAttribute("backPath","add");
        model.addAttribute("contractorID",0);
        model.addAttribute("contractor",contractorServices.newContractor());
        return "editContractor";
    }
    @GetMapping("/contractor")
    public String getContractors(Model model){
        model.addAttribute("welcome","Contractors");
        return "contractor";
    }
    @GetMapping("/contractor/0")
    public String setContractor(Model model){
        try {
            model.addAttribute("backPath","contractor");
            model.addAttribute("contractorID",0);
            model.addAttribute("contractor",contractorServices.newContractor());
            return "editContractor";
        }
        catch (Exception o){
            o.printStackTrace();
        }
        return "contractor";
    }

    @GetMapping("/contractor/edit/{contractorID}")
    public String setContractor(@PathVariable Long contractorID,Model model){
        model.addAttribute("backPath","contractor");
        model.addAttribute("contractorID",contractorID);
        model.addAttribute("contractor",contractorServices.getById(contractorID));
        return "editContractor";
    }

    @PostMapping("/contractor/edit/{contractorID}/{path}")
    public String getContractor(@PathVariable Long contractorID,
                         @PathVariable String path,
                         @RequestParam("file") MultipartFile file,
                         Contractor contractor,
                         Model model){
        try {
            contractor.setImage(Base64.encode(file.getBytes()).toCharArray());
            System.out.println(Base64.encode(file.getBytes()));
        }
        catch (Exception e){
            e.printStackTrace();
        }
        String check = contractorServices.saveContractor(contractor);
        if(check!=""){
            contractor.setImage(null);
            model.addAttribute("backPath",path);
            model.addAttribute("contractorID",contractorID);
            model.addAttribute("contractor",contractor);
            model.addAttribute("error",check);
            return "editContractor";
        }

        if(path.equals("add")){
            model.addAttribute("contractID","NEW");
            model.addAttribute("contract",contractorServices.newContractor());
            return "redirect:/add";
        }
        else return "contractor";
    }

    @GetMapping("/contractor/delete/{contractorID}")
    public String deleteContractor(@PathVariable Long contractorID){
        contractorServices.removeContract(contractorID);
        return "contractor";
    }

    @GetMapping("/contractor/reset")
    public String resetContract(){
        contractorServices.removeAll();
        return "index";
    }

}
