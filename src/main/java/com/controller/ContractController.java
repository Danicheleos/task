package com.controller;

import com.controller.services.ContractServices;
import com.domains.Contract;
import com.repositories.ContractRepository;
import com.repositories.StageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletResponse;
import java.util.List;


@Controller
public class ContractController {
    @Autowired
    private ContractServices contractServices;


    public ContractController(ContractRepository contractRepository, StageRepository stageRepository) {
        contractServices = new ContractServices(contractRepository,stageRepository);
    }




    @GetMapping("/")
    public String getMain(Model model){
        model.addAttribute("hi","Dan");
        return "index";
    }



    @GetMapping("/add")
    public String addContract(Model model){

        model.addAttribute("contractID","NEW");
        model.addAttribute("contract",contractServices.newContract());
        return "editContract";
    }


    @GetMapping("/edit/{contractID}")
    public String editContract(@PathVariable Long contractID, Model model){
        model.addAttribute("contractID",contractID);
        model.addAttribute("contract", contractServices.getById(contractID));
        return "editContract";
    }



    @PostMapping("/edit/{contractID}")
    public String editContract(@PathVariable String contractID, Contract contract, Model model){

        String check = contractServices.checkContract(contract);

        if (check!=""){
            model.addAttribute("error",check);
            model.addAttribute("contractID",contractID);
            model.addAttribute("contract",contract);
            return "editContract";
        }
        contractServices.saveContract(contract);
        return "index";
    }

    @GetMapping("/delete/{contractID}")
    public String deleteContract(@PathVariable Long contractID, Model model){
        contractServices.removeContract(contractID);
        return "index";
    }

    @GetMapping("/reset")
    public String resetContract(){
        contractServices.removeAll();
        return "index";
    }

    @PostMapping("/export")
    public String export(@RequestParam(name = "export", required = false) List<Long> contractIDs,
                         @RequestParam(name = "exportType") String exportType,
                         HttpServletResponse response) {
    return contractServices.export(contractIDs,exportType,response);
    }


}
