package com.controller.services;

import com.domains.Contract;
import com.domains.Stage;
import com.repositories.ContractRepository;
import com.repositories.StageRepository;
import com.validators.StageValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
@Service
public class StageServices {
    @Autowired
    private final ContractRepository contractRepository;
    @Autowired
    private final StageRepository stageRepository;
    @Autowired
    private final StageValidator stageValidator;

    public StageServices(ContractRepository contractRepository, StageRepository stageRepository) {
        this.contractRepository = contractRepository;
        this.stageRepository = stageRepository;
        this.stageValidator = new StageValidator(contractRepository,stageRepository);
    }


    public Stage newStage(Long contract_id){
        Stage n = Stage.newBuilder(contract_id)
                .setId(0L)
                .setName("")
                .setStart(new Date(System.currentTimeMillis()))
                .setEnd(new Date(System.currentTimeMillis()))
                .setCost(0.0)
                .setPurchaseDate(new Date(System.currentTimeMillis()))
                .build();
        return n;
    }

    public Stage getById(Long id){
        return stageRepository.findById(id).get();
    }

    public String  check(Stage stage){ return stageValidator.validation(stage);}

    public void save(Stage stage){
        stageRepository.save(stage);
        calcContract(stage.getContractId());
    }

    public void remove(Long id){
        Long c_id = stageRepository.findById(id).get().getContractId();
        stageRepository.deleteById(id);
        calcContract(c_id);
    }


    public void calcContract(Long contractID){
        Contract c = contractRepository.findById(contractID).get();
        c.setCost(0.0);
        for(Stage s: stageRepository.findAll()){
            if(s.getContractId()==c.getId()) c.addCost(s.getCost());
        }
        contractRepository.save(c);
    }
}
