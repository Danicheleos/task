package com.controller.services;

import com.domains.Contract;
import com.domains.Stage;
import com.repositories.ContractRepository;
import com.repositories.ContractorRepository;
import com.repositories.StageRepository;
import com.validators.ContractValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ContractServices {
    @Autowired
    private final ContractRepository contractRepository;
    @Autowired
    private final StageRepository stageRepository;
    @Autowired
    private final ContractValidator contractValidator;
    @Autowired
    ContractExporter exporter;
    @Autowired
    ContractorRepository contractorRepository;

    public ContractServices(ContractRepository contractRepository, StageRepository stageRepository) {
        this.contractRepository = contractRepository;
        this.stageRepository = stageRepository;
        this.contractValidator = new ContractValidator(stageRepository);
    }

    public Contract newContract(){
        Contract n = Contract.newBuilder()
                .setId(0L)
                .setName("")
                .setContractId(0L)
                .setDate(new Date(System.currentTimeMillis()))
                .setStart(new Date(System.currentTimeMillis()))
                .setEnd(new Date(System.currentTimeMillis()))
                .setCost(0.0)
                .build();
        return n;
    }

    public Contract getById(Long id){
        return contractRepository.findById(id).get();
    }

    public void saveContract(Contract contract){
        contract.setContractorId(contractorRepository.findByName(contract.getContractorName()).getId());
        contractRepository.save(contract);
    }

    public String checkContract(Contract contract){
        return contractValidator.validation(contract);
    }

    public void removeContract(Long id){
        for (Stage s: stageRepository.findAll()){
            if (s.getContractId()==id){
                stageRepository.delete(s);
            }
        }
        if (contractRepository.existsById(id))
            contractRepository.delete(contractRepository.findById(id).get());

    }

    public void removeAll(){
            stageRepository.deleteAll();
            contractRepository.deleteAll();
    }

    public String export(List<Long> contractIDs,String exportType, HttpServletResponse response) {

        if(contractIDs != null) {
            List<Contract> contracts = contractRepository.findAllById(contractIDs);
            File file = exporter.createFile(contracts, exportType);
            try {
                InputStream in = new FileInputStream(file);
                if(Objects.equals(exportType, "PDF")){
                    response.setContentType("application/pdf");
                } else {
                    response.setContentType("text/csv");
                }
                response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
                response.setHeader("Content-Length", String.valueOf(file.length()));
                FileCopyUtils.copy(in, response.getOutputStream());
            } catch (IOException e) {
            }
            return null;
        }
        else return "index";
    }


}
