package com.controller.services;

import com.domains.Contract;
import com.domains.Contractor;
import com.domains.Stage;
import com.repositories.ContractRepository;
import com.repositories.ContractorRepository;
import com.repositories.StageRepository;
import com.validators.ContractValidator;
import com.validators.ContractorValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class ContractorServices {
    @Autowired
    ContractRepository contractRepository;
    @Autowired
    ContractServices contractServices;
    @Autowired
    private StageRepository stageRepository;
    @Autowired
    private ContractorRepository contractorRepository;
    @Autowired
    private ContractValidator contractValidator;
    @Autowired
    private ContractorValidator contractorValidator;

    public Contractor newContractor(){
        Contractor contractor = new Contractor().newBuilder()
                .setId(0L)
                .setName("")
                .setImg(null)
                .setPhone("")
                .setAddress("")
                .build();
        return contractor;
    }

    public Contractor getById(Long id){
        return contractorRepository.findById(id).get();
    }

    public String saveContractor(Contractor contractor){

        String check = contractorValidator.validation(contractor);
        if(contractorValidator.validation(contractor)=="") contractorRepository.save(contractor);
        return check;
    }



    public void removeContract(Long id){
        for(Contract contract: contractRepository.findAll()){
            if(contract.getContractorId()==id){
                contractServices.removeContract(contract.getId());
            }
        }
        contractorRepository.delete(contractorRepository.findById(id).get());
    }

    public void removeAll(){
        contractServices.removeAll();
        contractorRepository.deleteAll();
    }

}
