package com.controller;

import com.domains.Contract;
import com.domains.Contractor;
import com.domains.Stage;
import com.repositories.ContractRepository;
import com.repositories.ContractorRepository;
import com.repositories.StageRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.io.File;
import java.nio.file.Files;

@RestController
@RequestMapping("/api")
public class ApiController {

    private final ContractRepository contractRepository;
    private final StageRepository stageRepository;
    private final ContractorRepository contractorRepository;
    public ApiController(ContractRepository contractRepository, StageRepository stageRepository,ContractorRepository contractorRepository) {
        this.contractRepository = contractRepository;
        this.stageRepository = stageRepository;
        this.contractorRepository=contractorRepository;
    }

    @GetMapping("/contracts")
    public Iterable<Contract> getContracts() {
        return contractRepository.findAll();
    }

    @GetMapping("/stages/{contractID}")
    public Iterable<Stage> getStages(@PathVariable Long contractID) {
        List<Stage> setStage = stageRepository.findAll();
        setStage.clear();
        for(Stage stage: stageRepository.findAll()){
            if(stage.getContractId().equals(contractID)) setStage.add(stage);
        }
        return setStage;
    }

    @GetMapping("/graph/{contractID}")
    public Iterable<Stage> getGraph(@PathVariable Long contractID) {
        List<Stage> setStage = stageRepository.findAll();
        setStage.clear();
        for(Stage stage: stageRepository.findAll()){
            if(stage.getContractId().equals(contractID)) setStage.add(stage);
        }
        return setStage;
    }

    @GetMapping("/contractors")
    public Iterable<Contractor> getContractors(){
        return contractorRepository.findAll();
    }
}
