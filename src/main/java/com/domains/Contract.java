package com.domains;



import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.sql.Date;
@Data
@Entity
@Table(name = "contract")
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id = 0L;

    @Column(name = "contractor_id")
    private Long contractorId = 0L;

    @Column(name = "name")
    private String name;

    @Column(name = "date")
    private Date date;

    @Column(name = "start")
    private Date start;

    @Column(name = "end")
    private Date end;

    @Column(name = "cost")
    private Double cost;

    @Getter
    @Setter
    private String contractorName = "";

    public void addCost(Double cost){
        this.cost+= cost;
    }


    public static Builder newBuilder() {
        return new Contract().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder setId(Long Id) {
           Contract.this.id = Id;
            return this;
        }

        public Builder setName(String contractorName) {
            Contract.this.name = contractorName;
            return this;
        }
        public Builder setContractId(Long contractorId) {
            Contract.this.contractorId = contractorId;
            return this;
        }

        public Builder setDate(Date date) {
            Contract.this.date = date;
            return this;
        }

        public Builder setStart(Date date) {
            Contract.this.start = date;
            return this;
        }

        public Builder setEnd(Date date) {
            Contract.this.end = date;
            return this;
        }

        public Builder setCost(Double cost) {
            Contract.this.cost = cost;
            return this;
        }
        public Contract build() {
            return Contract.this;
        }

    }



    public String toString(){
        return id.toString()+" "+ contractorId +" "+date.toString()+" "+start.toString()+" "+end.toString()+" "+cost.toString()+"\n";
    }

}
