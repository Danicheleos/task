package com.domains;

import lombok.Data;
import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@Table(name = "stage")
public class Stage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id = 0L;
    Long contractId;

    @Column(name = "name")
    String name = "";

    @Column(name = "start")
    Date start = new Date(0);

    @Column(name = "end")
    Date end=new Date(0);

    @Column(name = "cost")
    Double cost = 0.0;

    @Column(name = "purchase_date")
    Date purchaseDate = new Date(0);


    public Stage build() {
        Stage stage = new Stage();
        return stage;
    }

    public static Builder newBuilder(Long Id) {
        return new Stage().new Builder(Id);
    }

    public class Builder {

        private Builder(Long id) {
            Stage.this.contractId = id;
        }

        public Builder setId(Long Id) {
            Stage.this.id = Id;
            return this;
        }

        public Builder setContractId(Long id) {
            Stage.this.contractId = id;
            return this;
        }

        public Builder setName(String name) {
            Stage.this.name = name;
            return this;
        }
        public Builder setStart(Date date) {
            Stage.this.start = date;
            return this;
        }
        public Builder setEnd(Date date) {
            Stage.this.end = date;
            return this;
        }

        public Builder setCost(Double cost) {
            Stage.this.cost = cost;
            return this;
        }

        public Builder setPurchaseDate(Date date) {
            Stage.this.purchaseDate = date;
            return this;
        }

        public Stage build() {
            return Stage.this;
        }

    }


    public String toString(){
        return id.toString()+ ' '+contractId.toString()+ ' ' + name.toString() +' '+ start.toString() + ' ' + end.toString() + ' ' + cost.toString() + ' ' + purchaseDate.toString();
    }

}

