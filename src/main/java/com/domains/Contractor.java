package com.domains;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Data
@Entity
@Table(name = "contractor")
public class Contractor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id = 0L;

    @Column(name = "image")
    private char[] image;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "name")
    private String name;


    public static Builder newBuilder() {
        return new Contractor().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder setId(Long Id) {
            Contractor.this.id = Id;
            return this;
        }

        public Builder setImg(char[] img) {
            try {
                image = img;
            }
            catch (Exception e){
                e.printStackTrace();
            }
            return this;
        }

        public Builder setName(String name) {
            Contractor.this.name =name;
            return this;
        }

        public Builder setPhone(String phone) {
            Contractor.this.phone = phone;
            return this;
        }

        public Builder setAddress(String address) {
            Contractor.this.address = address;
            return this;
        }
        public Contractor build() {
            return Contractor.this;
        }

    }

}
